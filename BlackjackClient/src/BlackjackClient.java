import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.*;

public class BlackjackClient{

	// ---------------------------------------------
	// Parametry i elementy okienka
	// ---------------------------------------------
	private static JFrame daneGracza;		 // ramka do wpisywania nazwy gracza              //
    private static JTextField nazwaGracza; // pole tekstowe do zapisu nazwy uzytkownika     //
	
	private static JFrame turaGracza;        // ramka do informowania o turze gracza //

    private static JFrame postawZaklad;      // ramka do stawiania zakladow       //
	private static JSlider kwotaZakladu;	// suwak do wybierania kwoty zakładu //

    private static JFrame Rozgrywka;		 // ramka służąca do gry        //
	private static JButton Hit;				// przycisk do dobierania kart //
	private static JButton Stand;  		   // przycisk do spasowania      //
    private static JLabel Pieniadze;
	private static JLabel Zaklad;
    private static JLabel Pieniadze2;
    private static JLabel Zaklad2;
    private static JLabel graczKarty;
    private static JLabel graczPunkty;
    private static JTextPane inniGracze;
    private static String inni;
    //---------------------------------------------------
	private static JLabel KrupierKarty2;                 //wartosci danych etykiet
	private static JLabel GraczKarty2;
	private static JLabel KrupierPunkty2;
	private static JLabel GraczPunkty2;
    //---------------------------------------------------
    // internetowe
    //---------------------------------------------------
	private static Socket polaczenie;
	private static final String adresIp = null;
	private static final int port = 33033;
	private static BufferedReader czytaj;
	private static PrintWriter pisz;
	private static String inf;
	// --------------------------------------------------
	// dane gracza
	// --------------------------------------------------
	//private static GraczBlackJacka Gracz;
	private static String nick;
	private static int posiadanePieniadze=1000;
	private static int zaklad;
	
	 // ------------------------------------------------------------
    //  Ustawia GUI rozgrywki
    // ------------------------------------------------------------
    private static void ustawPanel (Container panel) {

    	inf= "";
    	GridBagConstraints cp = new GridBagConstraints();
    	panel.setBackground (Color.green);
    	panel.setLayout (new GridBagLayout());
    	cp.fill = GridBagConstraints.HORIZONTAL;
    	// --------------------------------
    	// przycisk Hit, do pobierania kart
    	// --------------------------------
    	Hit = new JButton ("Hit!");
    	cp.ipady=10;
    	cp.ipadx=20;
       	cp.weightx=1;
    	cp.weighty=2;
    	cp.gridheight=2;
    	cp.fill = GridBagConstraints.HORIZONTAL;
    	cp.gridx=0;
    	cp.gridy=0;
    	cp.insets=new Insets(0,5,0,5);
    	Hit.addActionListener (new HitButtonListener());
    	Hit.setEnabled(false);
    	panel.add (Hit,cp);
    	// --------------------------------
    	// karty posiadane przez krupiera
    	// --------------------------------
        JLabel krupierKarty = new JLabel("Karty Krupiera: ");
    	cp.ipady=0;
    	cp.ipadx=0;
        cp.insets=new Insets(0,0,0,0);
       	cp.weightx=1;
    	cp.weighty=1;
    	cp.gridheight=1;
    	cp.gridx=1;
    	cp.gridy=0;
    	cp.fill = GridBagConstraints.HORIZONTAL;
    	panel.add (krupierKarty,cp);
       	KrupierKarty2 = new JLabel ("0");
       	cp.weightx=1;
    	cp.weighty=1;
    	cp.gridheight=1;
    	cp.gridx=1;
    	cp.gridy=1;
    	cp.fill = GridBagConstraints.HORIZONTAL;
    	panel.add (KrupierKarty2,cp);
    	// --------------------------------
    	// punkty posiadane przez krupiera
    	// --------------------------------
        JLabel krupierPunkty = new JLabel("Punkty Krupiera: ");
       	cp.weightx=1;
    	cp.weighty=1;
    	cp.gridheight=1;
    	cp.fill = GridBagConstraints.HORIZONTAL;
    	cp.gridx=2;
    	cp.gridy=0;
    	panel.add (krupierPunkty,cp);
    	KrupierPunkty2= new JLabel ("0");
       	cp.weightx=1;
    	cp.weighty=1;
    	cp.gridheight=1;
    	cp.fill = GridBagConstraints.HORIZONTAL;
    	cp.gridx=2;
    	cp.gridy=1;
    	panel.add (KrupierPunkty2,cp);
    	// --------------------------------
    	// przycisk Stand, do pasowania
    	// i przerwania swojej tury
    	// --------------------------------
    	Stand = new JButton ("Stand!");
    	cp.ipady=10;
    	cp.ipadx=20;
       	cp.weightx=1;
    	cp.weighty=3;
    	cp.gridheight=2;
        cp.insets=new Insets(0,5,0,5);
    	cp.fill = GridBagConstraints.HORIZONTAL;
    	cp.gridx=0;
    	cp.gridy=2;
    	Stand.addActionListener (new StandButtonListener());
    	Stand.setEnabled(false);
    	panel.add (Stand,cp);
    	// --------------------------------
    	// karty posiadane przez gracza
    	// --------------------------------
        graczKarty = new JLabel("Karty " + nick);
    	cp.ipady=0;
    	cp.ipadx=0;
       	cp.weightx=1;
    	cp.weighty=1;
    	cp.gridheight=1;
    	cp.fill = GridBagConstraints.HORIZONTAL;  
    	cp.gridx=1;
    	cp.gridy=2;
    	panel.add (graczKarty,cp);
    	GraczKarty2 = new JLabel ("0"); 
       	cp.weightx=1;
    	cp.weighty=1;
    	cp.gridheight=1;
    	cp.fill = GridBagConstraints.HORIZONTAL;  
    	cp.gridx=1;
    	cp.gridy=3;
    	panel.add (GraczKarty2,cp); 
    	// --------------------------------
    	// karty posiadane przez gracza
    	// --------------------------------
        graczPunkty = new JLabel("Punkty " + nick);
       	cp.weightx=1;
    	cp.weighty=1;
    	cp.gridheight=1;
    	cp.fill = GridBagConstraints.HORIZONTAL;
    	cp.gridx=2;
    	cp.gridy=2;
    	panel.add (graczPunkty,cp);
    	GraczPunkty2 = new JLabel ("0");
       	cp.weightx=1;
    	cp.weighty=1;
    	cp.gridheight=1;
    	cp.fill = GridBagConstraints.HORIZONTAL;
    	cp.gridx=2;
    	cp.gridy=3;
    	panel.add (GraczPunkty2,cp);
    	// --------------------------------
    	// pieniadze posiadane przez gracza
    	// --------------------------------
    	Pieniadze = new JLabel ("Posiadane pieniadze: " + posiadanePieniadze);
       	cp.weightx=1;
    	cp.weighty=1;

    	cp.gridwidth=2;
    	cp.gridheight=1;
    	cp.fill = GridBagConstraints.HORIZONTAL;
    	cp.gridx=0;
    	cp.gridy=4;
    	panel.add (Pieniadze,cp);
    	// --------------------------------
    	// zaklad postawiony przez gracza
    	// --------------------------------
    	Zaklad = new JLabel ("Zaklad: " + zaklad);
       	cp.weightx=1;
    	cp.weighty=1;
    	cp.gridheight=1;
    	cp.fill = GridBagConstraints.HORIZONTAL;
    	cp.gridx=2;
    	cp.gridy=4;
    	panel.add (Zaklad,cp);
    	// ---------------------------------
        // informacja o innych grazach
        // ---------------------------------
        JLabel inniGraczeT = new JLabel("Pozostali gracze:");
        cp.weightx=1;
        cp.weighty=1;
        cp.gridheight=1;
        cp.gridwidth= 1;
        cp.fill = GridBagConstraints.HORIZONTAL;
        cp.gridx=6;
        cp.gridy=0;
        panel.add (inniGraczeT,cp);

        inni = "";
        inniGracze = new JTextPane();
        inniGracze.setText(inni);
        inniGracze.setEditable(false);
        cp.ipady=50;
        cp.ipadx=30;
        cp.weightx=2;
        cp.weighty=3;
        cp.gridheight=6;
        cp.gridwidth= 3;
        cp.fill = GridBagConstraints.HORIZONTAL;
        cp.gridx=6;
        cp.gridy=1;
        panel.add (inniGracze,cp);
    }
	 // ------------------------------------------------------------
    //  Ustawia GUI pzekazywania nazwy
    // ------------------------------------------------------------
    private static void zapiszDaneGracza(Container panel) {
    	panel.setBackground (Color.white);
    	panel.setLayout (new BorderLayout());
        JButton zapisz = new JButton("Zapisz");
    	panel.add(zapisz, BorderLayout.PAGE_END);
    	nazwaGracza = new JTextField("Gracz");
    	nazwaGracza.setPreferredSize(new Dimension(500, 50));
    	panel.add(nazwaGracza, BorderLayout.PAGE_START);
    	zapisz.addActionListener(new ZapiszButtonListener());
    }
	 // ------------------------------------------------------------
    //  Ustawia GUI informujace o turze gracza
    // ------------------------------------------------------------
    private static void ustawPoinformowanie (Container panel) {
    	panel.setBackground (Color.white);
    	panel.setLayout (new BorderLayout());
        JButton ok = new JButton("Twoja tura!");
    	ok.addActionListener(new OkButtonListener());
    	panel.add(ok, BorderLayout.CENTER);
    	turaGracza.setSize(200, 100);
    }
	 // ------------------------------------------------------------
    //  Ustawia GUI do ustawienia stawianego zakladu
    // ------------------------------------------------------------
    private static void ustawZaklad(Container panel) {
    	panel.setBackground (Color.white);
    	panel.setLayout (new BorderLayout());
        JButton postaw = new JButton("Postaw zaklad");
    	postaw.addActionListener(new PostawButtonListener());
    	panel.add(postaw, BorderLayout.PAGE_END);
    	kwotaZakladu= new JSlider();
    	kwotaZakladu.setMaximum(posiadanePieniadze);
    	kwotaZakladu.setMinimum(0);
    	kwotaZakladu.setMajorTickSpacing(10);
    	kwotaZakladu.setPaintTicks(true);
    	panel.add(kwotaZakladu, BorderLayout.PAGE_START);
    	// --------------------------------
    	// pieniadze posiadane przez gracza
    	// --------------------------------
    	Pieniadze2 = new JLabel("Posiadane pieniadze: " + posiadanePieniadze);
    	panel.add (Pieniadze2, BorderLayout.LINE_START);
    	// --------------------------------
    	// zaklad postawiony przez gracza
    	// --------------------------------
    	Zaklad2 = new JLabel("Zaklad: " + zaklad);
    	panel.add (Zaklad2,BorderLayout.LINE_END);
    	
    }
    // *******************************************************************
    //  Reprezentuje listener dla akcji wcisniecia przycisku "zapisz" 
    // *******************************************************************
    private static class ZapiszButtonListener implements ActionListener
    {
        public void actionPerformed (ActionEvent event)
        {
        	String nazwa = nazwaGracza.getText();
            if(nazwa.length()!=0){						//jezeli jest tekst w polu tekstowym
            	nick = nazwa;		 				   // ustaw go jako nazwe gracza,
            	daneGracza.setVisible(false);		  // ustaw panel z nazwa jako niewidoczy
                Rozgrywka.pack();
            	Rozgrywka.setVisible(true);				 // oraz ustaw panel do gry jako widoczny
                ustawZaklad(postawZaklad.getContentPane());
                postawZaklad.setVisible(false);
                postawZaklad.pack();
            	pisz.println(nazwa);
                graczPunkty.setText("Punkty " + nick);
                graczKarty.setText("Karty " + nick);
                Rozgrywka.repaint();
            }
        }
    }
    // *******************************************************************
    //  Reprezentuje listener dla akcji wcisniecia przycisku "Hit"
    //  który służy do dobierania kart
    // *******************************************************************
    private static class HitButtonListener implements ActionListener
    {
        public void actionPerformed (ActionEvent event)		
        {
        	pisz.println("tak");
        }
    }
    // *******************************************************************
    //  Reprezentuje listener dla akcji wcisniecia przycisku "Stand"
    //  który służy do zakończenia swojej tury
    // *******************************************************************
    private static class StandButtonListener implements ActionListener
    {
        public void actionPerformed (ActionEvent event)		
        {
        	Hit.setEnabled(false);
        	Stand.setEnabled(false);
        	pisz.println("nie");
        }
    }
    // *******************************************************************
    //  Reprezentuje listener dla akcji wcisniecia przycisku "Stand"
    //  który służy do zakończenia swojej tury
    // *******************************************************************
    private static class OkButtonListener implements ActionListener
    {
        public void actionPerformed (ActionEvent event)		
        {
        	turaGracza.setVisible(false);
        }
    }    
    // *******************************************************************
    //  Reprezentuje listener dla akcji wcisniecia przycisku "Postaw"
    //  który służy do stawiania pewnej kwoty zakładu
    // *******************************************************************
    private static class PostawButtonListener implements ActionListener
    {
        public void actionPerformed (ActionEvent event)		
        {
        	zaklad=kwotaZakladu.getValue();
        	posiadanePieniadze=posiadanePieniadze-zaklad;
        	postawZaklad.setVisible(false);
        	System.out.print(zaklad);
        	pisz.println(kwotaZakladu.getValue());
            Pieniadze.setText("Posiadane pieniadze: " + posiadanePieniadze);
            Zaklad.setText("Zaklad: "+ zaklad);
        	}
    }
    private static void stworzGUI() {
        Rozgrywka  = new JFrame("Blackjack");
        daneGracza = new JFrame("Nazwa użytkownika");
        turaGracza = new JFrame("Potwierdzenie");
        postawZaklad= new JFrame("Zaklad");
        ustawPanel(Rozgrywka.getContentPane());
        ustawPoinformowanie(turaGracza.getContentPane());
        zapiszDaneGracza(daneGracza.getContentPane());
        Rozgrywka.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        daneGracza.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        postawZaklad.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        postawZaklad.pack();
        daneGracza.pack();
        daneGracza.setVisible(true);

    }
    // **********************************
    // nawiazuje polaczenie z serwerem
    // **********************************
    private static void nawiazPolaczenie(){

		try {
			polaczenie= new Socket(adresIp, port);
		} catch (IOException e) {e.printStackTrace();}

	}
    // **************************************
    // ustawia strumienia do odczytu i zapisu
    // **************************************
    private static void ustawStrumienie(){
    	try {
			czytaj = new BufferedReader( new InputStreamReader(polaczenie.getInputStream()));
			pisz = new PrintWriter(polaczenie.getOutputStream(), true);
		} catch (IOException e) {e.printStackTrace();}
    }
    // *****************************
    // czytanie zapytań z serwera
    // *****************************
    private static class czytajZSerwera extends Thread{
    	public void run() {

			try {
				while (true) {
					inf = czytaj.readLine();
					if(inf!=null) switch (inf) {
                        case "tura":
                            // tu informacja o nowej turze
                            turaGracza.setVisible(true);
                            break;
                        case "enable":
                            Hit.setEnabled(true);
                            Stand.setEnabled(true);
                            break;
                        case "zaklad":
                            // tu wprowadzenie kwoty
                            kwotaZakladu.setMaximum(posiadanePieniadze);
                            kwotaZakladu.setMinimum(0);
                            kwotaZakladu.setMajorTickSpacing(10);
                            Pieniadze2.setText("Posiadane pieniadze " +posiadanePieniadze);
                            postawZaklad.repaint();
                            postawZaklad.setVisible(true);
                            break;
                        case "fura":
                            Hit.setEnabled(false);
                            Stand.setEnabled(false);
                            GraczPunkty2.setText("Fura");
                            break;
                        case "karty":
                            // aktualizacja rozdanych kart
                            GraczKarty2.setText(czytaj.readLine());
                            break;
                        case "krupier":
                            // aktualizacja kart i punktów krupiera
                            KrupierKarty2.setText(czytaj.readLine());
                            KrupierPunkty2.setText(czytaj.readLine());
                            break;
                        case "krupierk":
                            // aktualizacja kart i punktów krupiera
                            KrupierKarty2.setText(czytaj.readLine());
                            break;
                        case "krupierp":
                            // aktualizacja kart i punktów krupiera
                            KrupierPunkty2.setText(czytaj.readLine());
                            break;
                        case "wynik":
                            // aktualizacja wyniku
                            GraczPunkty2.setText(czytaj.readLine());
                            break;
                        case "blackjack":
                            // tu informacja o rozdanych kartach
                            Hit.setEnabled(false);
                            Stand.setEnabled(false);
                            break;
                        case "pieniadze":
                            // aktualizuje stan konta gracza
                            posiadanePieniadze = Integer.parseInt(czytaj.readLine());
                            Pieniadze.setText("Pieniadze " + posiadanePieniadze);
                            Zaklad.setText("Zaklad 0");
                            GraczPunkty2.setText("0");
                            GraczKarty2.setText("0");
                            KrupierPunkty2.setText("0");
                            KrupierKarty2.setText("0");
                            inni = "";
                            inniGracze.setText(inni);
                            break;
                        case "inni":
                            // aktualizowanie kart innych graczy
                            inni =(inni + czytaj.readLine()+ "\n");
                            inniGracze.setText(inni);
                            inniGracze.repaint();
                            Rozgrywka.repaint();
                            break;
                        default:
                            System.out.println("Cos jest nie tak " + inf);
                            break;
                    }
				}
			}catch (IOException e) {e.printStackTrace();}
    	}
    }
    
    public static void main(String[] args) {
        nawiazPolaczenie();
        ustawStrumienie();
        javax.swing.SwingUtilities.invokeLater(BlackjackClient::stworzGUI);
        new czytajZSerwera().start();
    }
    protected void finalize(){
    	try {
    	    System.out.print("finalize");
			pisz.close();
			czytaj.close();
			polaczenie.close();
		} catch (IOException e) {e.printStackTrace();}
    }
}
