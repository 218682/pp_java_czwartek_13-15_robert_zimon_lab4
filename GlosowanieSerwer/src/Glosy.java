// klasa zliczajaca glosy oddane na poszczegolnych kandydatow
public class Glosy {
	private static int glosyDlaJacek;
	private static int glosyDlaPlacek;
	public static void glosDlaJacka(){
		glosyDlaJacek++;
	}
	public static void glosDlaPlacka(){
		glosyDlaPlacek++;
	}
	public static int getGlosyDlaJacek(){
		return glosyDlaJacek;
	}
	public static int getGlosyDlaPlacek(){
		return glosyDlaPlacek;
	}
}
