import java.io.*;
import java.net.*;

public class GlosowanieSerwer {
	
	private static int portNr = 3333;
	private static ServerSocket ssc;

    
    private static Socket polaczenie;
	
	public static void main(String[] args) {
		try {
			ssc = new ServerSocket(portNr);
			System.out.println("Serwer utworzony");
		while(true){
			czekajNaPolaczenie();
			polaczenie = null;
		}
		} catch (IOException e) {e.printStackTrace(); }
	}

	private static void czekajNaPolaczenie() throws IOException{
		polaczenie= new Socket();
		System.out.println("Czekam na polaczenie");
		polaczenie = ssc.accept();
		System.out.println("Polaczono z "+polaczenie.getInetAddress().getHostAddress());
		new NowyWyborca(polaczenie).start();
	}
}
