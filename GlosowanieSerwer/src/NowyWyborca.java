import java.io.*;
import java.net.*;

public class NowyWyborca extends Thread{

	protected Socket wyborca;
	private BufferedReader rdr;
	private String inf;
	private PrintWriter wrt;
	
	public NowyWyborca(Socket nowePolaczenie){
		wyborca= new Socket();
		this.wyborca=nowePolaczenie;
	}
	
	public void run() {
			try {
		    	ustawStrumienie();
				while(true){
					czytaj();
				}
			} catch (IOException e) {return;}  //czytaj co wysyla wyborca

	}
	
    private void ustawStrumienie() throws IOException{
		rdr = new BufferedReader(new InputStreamReader(wyborca.getInputStream())); //do czytania
		wrt = new PrintWriter(wyborca.getOutputStream(), true);                       // do pisania
    }
    // *****************************************
    // czytaj dane z aplikacji
    // *****************************************
    private void czytaj() throws IOException{

		System.out.println("czekam na dane");

    	while((inf=rdr.readLine())!=null)
    	{
    		if(inf.equals("Placek"))
    			Glosy.glosDlaPlacka();
    		else if(inf.equals("Jacek"))
    			Glosy.glosDlaJacka();
    		else if(inf.equals("Odswiez"))
    			wysylaj();
    		else
    			System.out.println("Cos jest nie tak " + inf);
       	}

    	
    }
    // ************************************************
    // wysyłaj dane do aplikacji klienta
    // ************************************************
    private void wysylaj() throws IOException{
    	wrt.println("Glosy dla Jacka: " + Glosy.getGlosyDlaJacek());
    	wrt.println("Glosy dla Placka: " + Glosy.getGlosyDlaPlacek());
    }
    protected void finalize(){
    	try {
			wrt.close();
			rdr.close();
			wyborca.close();
		} catch (IOException e) {e.printStackTrace();}
    }
}
