// ***********************************************************************
//  LicznikGlosow.java
//
//  Wykorzystuje GUI oraz listenery eventow do glosowania 
//  na dwoch kandydatow -- Jacka i Placka.
//
// ***********************************************************************

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import javax.swing.*;

public class Glosowanie extends JApplet{

	private static final long serialVersionUID = 1L;

	private int APPLET_WIDTH = 300, APPLET_HEIGHT=80;
    private JLabel labelJacek;
    private JButton Jacek;
    private JLabel labelPlacek;
    private JButton Placek;
    //------------------------------------------------------
    // internetowe
    //---------------------------------------------------
	private Socket polaczenie;
	private final String adresIp = null;
	private final int port = 3333;
	private BufferedReader rdr;
	private PrintWriter wrt;
	private String inf;
	
    // ------------------------------------------------------------
    //  Ustawia GUI i początkowe wartości
    // ------------------------------------------------------------
    public void init () {
    	nawiazPolaczenie();
    	ustawStrumienie();
    	inf= new String();
    	Jacek = new JButton ("Glosuj na Jacka!");
    	Jacek.addActionListener (new JacekButtonListener());
    	Placek = new JButton ("Glosuj na Placka!");
    	Placek.addActionListener (new PlacekButtonListener());
    	
    	labelJacek = new JLabel ("Glosy dla Jacka: ");
    	labelPlacek = new JLabel ("Glosy dla Placka: ");
    	Container cp = getContentPane();
    	cp.setBackground (Color.cyan);
    	cp.setLayout (new FlowLayout());
    	cp.add (Jacek);
    	cp.add (labelJacek);
    	cp.add (Placek);
    	cp.add (labelPlacek);
    	setSize (APPLET_WIDTH, APPLET_HEIGHT);
    	ustalGlosy(); // odczytaj dane z serwera
    	new czytaj().start(); //utwórz nowy wątek do odczytywania odpowiedzi od serwera
    }

    // *******************************************************************
    //  Reprezentuje listener dla akcji wcisniecia przycisku
    // *******************************************************************
    private class JacekButtonListener implements ActionListener
    {
        public void actionPerformed (ActionEvent event)
        {
        	wrt.println("Jacek");
        	wrt.println("Odswiez");
        }
    }
    // *******************************************************************
    //  Reprezentuje listener dla akcji wcisniecia przycisku
    // *******************************************************************
    private class PlacekButtonListener implements ActionListener
    {
        public void actionPerformed (ActionEvent event)
        {
        	wrt.println("Placek");
        	wrt.println("Odswiez");
        	//try {
				//czytaj();
			//} catch (IOException e) {e.printStackTrace();}
        }
    }
    
    // **********************************************
    // odczytuje poczatkowe wartosci glosow z serwera
    // *********************************************
    private void ustalGlosy(){
    	wrt.println("Odswiez");
        repaint ();
    }
    
    // **********************************
    // nawiazuje polaczenie z serwerem
    // **********************************
    private void nawiazPolaczenie(){
    	try {
			polaczenie= new Socket(adresIp, port);
		} catch (UnknownHostException e) { e.printStackTrace();} 
    	  catch (IOException e) {e.printStackTrace();}
    }
    
    // **************************************
    // ustawia strumienia do odczytu i zapisu
    // **************************************
    private void ustawStrumienie(){
    	try {
			rdr = new BufferedReader( new InputStreamReader(polaczenie.getInputStream()));
			wrt = new PrintWriter(polaczenie.getOutputStream(), true);
		} catch (IOException e) {e.printStackTrace();}
    }
    
    protected void finalize(){
    	try {
			wrt.close();
			rdr.close();
			polaczenie.close();
		} catch (IOException e) {e.printStackTrace();}
    }
    
    // *****************************
    // czytanie odpowiedzi z serwera
    // *****************************
    private class czytaj extends Thread{
    	public void run(){
    		
    	try {
			while(true) {
				if(!rdr.ready()){
					Thread.sleep(250);
					if(!rdr.ready())
					wrt.println("Odswiez");
				}
				inf = rdr.readLine();
				if(inf.startsWith("Glosy dla Placka: ")){
					labelPlacek.setText (inf); 
					repaint();
				}
				else if(inf.startsWith("Glosy dla Jacka: ")){
			        labelJacek.setText (inf); 
			        repaint();
				}
				else
					System.out.println("Cos jest nie tak " + inf);
				}
		} catch (IOException | InterruptedException e) {e.printStackTrace();}
    	}
    }
}
