import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

import static java.lang.Thread.sleep;

// ******************************************
// Blackjack wersja serwerowa
// ******************************************
public class BlackjackServer {
	private static List<GraczBlackJacka> gracze     = new LinkedList<>();
	private static GraczBlackJacka Krupier    	    = new GraczBlackJacka();
	private static int iloscGraczy;
	private static int iloscNowych;
	private static Talia talia 						= new Talia();
    //---------------------------------------------------
    // internetowe
    //---------------------------------------------------
	private static LinkedList<Socket> polaczenie;
	private static ServerSocket serwer;
	private static final int port = 33033;
	private static LinkedList<BufferedReader> czytaj;
	private static LinkedList<PrintWriter> pisz;

	/** 
	 * @param nrGracza - id gracza którego dotyczy tura
	 * @return wynik - metoda zwraca wartosc punktow jaka uzyskal gracz o ile ten nie przekroczyl wartosci 21
	 * w takim wypadku wartoscia zwrocona bedzie -1
	 * -1 zwrócone zostanie rónierz gdy gracz opóścił rozgrywke przed czasem
	 */
	private static int turaGracza(int nrGracza) throws IOException {
		boolean Hit = true;     // czy gracz chce kontynuowac gre i dobrac karte
		int wynik	= -1;       // zmienna pomocnicza do zapisywania wyniku gracza
		String czyDobierac;
		pisz.get(nrGracza).println("tura");
		while(Hit) {
			do{
				System.out.println("Czy chcesz dobrac karte? [t]/[n] [tak]/[nie]");
				pisz.get(nrGracza).println("enable");
				czyDobierac=czytaj.get(nrGracza).readLine();
				if(czyDobierac.equals("t") || czyDobierac.equals("tak")){
					Hit=true;
				}
				if(czyDobierac.equals("n") || czyDobierac.equals("nie")){
					Hit=false;
				}
			} while(!czyDobierac.equals("t") && !czyDobierac.equals("n") 			//przy poprawnie działającej
					&& !czyDobierac.equals("tak")  && !czyDobierac.equals("nie")); //wersji klienta nie jest potrzebne
			if(Hit) {
				gracze.get(nrGracza).dodajKarte(talia.losujKarte());    // wylosuj karte dla gracza
				wynik = gracze.get(nrGracza).wyliczPunkty();
				pisz.get(nrGracza).println("karty");
				for(int j=0; j<gracze.get(nrGracza).getIloscKart(); j++) {
					try {
						System.out.print(gracze.get(nrGracza).get(j).getFigura()
								+ "" + gracze.get(nrGracza).get(j).getKolor() + " ");
						pisz.get(nrGracza).print(gracze.get(nrGracza).get(j).getFigura()
								+ "" + gracze.get(nrGracza).get(j).getKolor() + " ");
					} catch (Exception e) {	e.printStackTrace();}
				}
				pisz.get(nrGracza).println();

				System.out.println();
				System.out.println("Wynik: " + wynik);
				pisz.get(nrGracza).println("wynik");
				pisz.get(nrGracza).println(wynik);
				if(wynik>21){
					pisz.get(nrGracza).println("fura");
					return -1;
				}
			}
			else {
				wynik = gracze.get(nrGracza).wyliczPunkty();
				return wynik;
			}
		}
		return wynik;
		
	}
	/** 
	 * Krupier zgodnie z zasadami gry dobiera karty jezeli jego wartosc puntowa nie przekracza 16
	 * czyli nie jest równa 17 lub wiecej, gdy osiągnął wynik większy lub równy 17 kończy dobieranie
	 * @return wynik - metoda zwraca wartosc punktow jaka uzyskal krupier, 
	 * chyba ze przekroczył 21 punktów, wtedy zwracaną wartością jest -1
	 */
	private static int turaKrupiera(){
		int wynik=-1;
		try {
			wynik=Krupier.wyliczPunkty();
			System.out.print("Karty krupiera to " + Krupier.get(0).getFigura() + "" + Krupier.get(0).getKolor()
							+ "  " + Krupier.get(1).getFigura() + "" + Krupier.get(1).getKolor() + "  ");
				while(wynik<17){
					Krupier.dodajKarte(talia.losujKarte());    // wylosuj karte dla krupiera
					wynik=Krupier.wyliczPunkty();
					System.out.print(Krupier.get(Krupier.getIloscKart()-1).getFigura()
									+ "" + Krupier.get(Krupier.getIloscKart()-1).getKolor() + "  ");
					if(wynik>21)
						return -1;
					if(wynik>=17)
						return wynik;					
				}
				return wynik;
		} catch (Exception e) {	e.printStackTrace();}
		return wynik;
	}
	/**
	 * pierwsze rozdanie kart
	 * 
	 * @param nrGracza - - id gracza którego dotyczy tura
	 */
	private static void rozdanie(int nrGracza){
		try {
			gracze.get(nrGracza).noweRozdanie();				    // wyczysc karty z reki gracza
			gracze.get(nrGracza).dodajKarte(talia.losujKarte());   // wylosuj dwie karty dla gracza i wyświetl je gracza
			gracze.get(nrGracza).dodajKarte(talia.losujKarte());
		} catch (Exception e) {	e.printStackTrace(); }
	 }
	/**
	 * pierwsze rozdanie kart dla krupiera, wyswietlana jest tylko druga z wylosowanych kart
	 */
	private static void rozdanieKrupier(){
		try {
			Krupier.noweRozdanie();					  // wyczysc karty z reki krupiera
			Krupier.dodajKarte(talia.losujKarte());  // wylosuj dwie karty dla krupiera
			Krupier.dodajKarte(talia.losujKarte());
		} catch (Exception e) {	e.printStackTrace();	}
	 }

	/**
	 * Klasa czekająca na nowych graczy, po próbie połączenie zapoczątkowanej przez klienta
	 * ustawiane jest połączenie, a gracz jest ustawiany w kolejce do swojej tury
	 */
	public static class NowyGracz extends Thread{						   //wątek czekający na połączenie z graczem
		public void run() {
			int i;
			while (true) {
				System.out.println("Czekam na polaczenie");
				try {
					polaczenie.add(serwer.accept());                       // czekaj na połączenie z graczem
					i=iloscGraczy+iloscNowych;
					ustawStrumienie(i);
					GraczBlackJacka nowyGracz = new GraczBlackJacka();
					nowyGracz.setNazwa(czytaj.get(i).readLine());      // czekaj na podanie nicku gracza
					gracze.add(nowyGracz);		                      // i dodaj gracza do listy
					iloscNowych++;									 // powiększ ilość oczekujących graczy
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/** Klasa wysyła podanemu graczowi informacje o posiadanych kartach przez krupiera
	 *
	 */
	public static class WyslijGraczowi extends Thread{
		private PrintWriter pisanie; // osobny strumien dla wątku by można było pisać do wszystkich graczy
		                            // w tym samym czasie
		private String doWyslania;
		WyslijGraczowi(int i){
			try {
				doWyslania = "";
				pisanie = new PrintWriter(polaczenie.get(i).getOutputStream(), true);
			} catch (IOException e) {e.printStackTrace();}
		}
		public void run() {
			try {
				for(int x=0; x<Krupier.getIloscKart(); x++){
					pisanie.println("krupierk");
					doWyslania=(doWyslania + Krupier.get(x).getFigura()+ "" +Krupier.get(x).getKolor()+ " ");
					pisanie.println(doWyslania);
					sleep(1000);
				}
				pisanie.println("krupierp");
				pisanie.println(Krupier.wyliczPunkty());
				return;
			} catch (Exception e) {e.printStackTrace();}
		}
	}
	public static void main(String[] args) {
		iloscGraczy			     = 0; 
		iloscNowych				 = 0;
		int zaklad;
		pisz					 = new LinkedList<>();
		czytaj                   = new LinkedList<>();
		polaczenie				 = new LinkedList<>();
		try {
		serwer  				 = new ServerSocket(port);	
		} catch (IOException e1) {e1.printStackTrace();	}
		new NowyGracz().start();

		int[] wynikGraczy;
		int wynikKrupiera;
		while(true){
			try {
				sleep(1000);
			} catch (InterruptedException e) {e.printStackTrace();}
			// ######################  rozgrywka ###########################
			iloscGraczy+=iloscNowych; 					 //przypisz nowych graczy do następnej tury
			iloscNowych=0;
			wynikGraczy        = new int[iloscGraczy]; //stworz nową tablice do przechowywania wyników
			wynikKrupiera      = 0;                   // zeruj wynik krupiera
			// ---------- stawianie zakładów i rozdawanie kart ----------------------------
			for(int i=iloscGraczy-1; i>=0; i--){
				try {
					if(gracze.get(i).getPieniadze()<1){   //gdy gracz juz nie ma pieniedzy
						gracze.remove(i);                // jest usuwany z listy graczy
						iloscGraczy--;
						polaczenie.remove(i);
						pisz.remove(i);
						czytaj.remove(i);
					}
					else {
						System.out.println(gracze.get(i).getNazwa());
						System.out.println("Jaki zaklad chcesz postawic? (0 by odejść od stołu) Twoje fundusze: " + gracze.get(i).getPieniadze());
						pisz.get(i).println("zaklad");
						zaklad= Integer.parseInt(czytaj.get(i).readLine());
					
						if(zaklad>0){
							gracze.get(i).setZaklad(zaklad);
						}
						else {
							System.out.print("jeden z graczy zakonczyl gre \n");
							gracze.remove(i); 			// gracz moze przerwać gre i opóścić stół
							iloscGraczy--;
							polaczenie.remove(i);
							pisz.remove(i);
							czytaj.remove(i);
						}
					}
				} catch (IOException e) {
					gracze.remove(i);
					polaczenie.remove(i);
					pisz.remove(i);
					czytaj.remove(i);
					iloscGraczy--;
				}
			}
			for(int i=iloscGraczy-1; i>=0; i--){ //rozdaj karty graczom i wyślij im co mają
				try {
					System.out.println(gracze.get(i).getNazwa());
					rozdanie(i);
					System.out.print(gracze.get(i).get(0).getFigura() + "" + gracze.get(i).get(0).getKolor() + " ");
					System.out.println( gracze.get(i).get(1).getFigura() + "" + gracze.get(i).get(1).getKolor());
					System.out.println("Punkty: " + gracze.get(i).wyliczPunkty()); // wyswietl wynik
					pisz.get(i).println("karty");
					pisz.get(i).println(gracze.get(i).get(0).getFigura() + "" + gracze.get(i).get(0).getKolor() + " "
								+ gracze.get(i).get(1).getFigura() + "" + gracze.get(i).get(1).getKolor());
					pisz.get(i).println("wynik");
					pisz.get(i).println(gracze.get(i).wyliczPunkty());
				} catch (Exception e) {
					gracze.remove(i);
					polaczenie.remove(i);
					pisz.remove(i);
					czytaj.remove(i);
					iloscGraczy--;
				}
			}
			// ---------------------------------------------------------------------------
			if(iloscGraczy!=0) {    //jeśli nie ma nikogo przy stole krupier nie gra
				rozdanieKrupier();	// w przeciwnym wypadku rozda sobie karty
					                // i pokarze graczom swoją drugą karte
				try {
					System.out.println("Karty krupiera: ?? " + Krupier.get(1).getFigura() + "" + Krupier.get(1).getKolor());
				} catch (Exception e) {	e.printStackTrace();}
				for(int i=iloscGraczy-1; i>=0; i--) {
					try {
						pisz.get(i).println("krupier");
						pisz.get(i).println("?? " +Krupier.get(1).getFigura() + "" + Krupier.get(1).getKolor());
						pisz.get(i).println("??");
					} catch (Exception e) {
						gracze.remove(i);
						polaczenie.remove(i);
						pisz.remove(i);
						czytaj.remove(i);
						iloscGraczy--;
					}
				}

			}
			// ---------- dobieranie kart - rozgrywka wlasciwa -------------------------
			for(int i=iloscGraczy-1; i>=0; i--){
				try {
					System.out.print(gracze.get(i).getNazwa() + ": ");
					System.out.print(gracze.get(i).get(0).getFigura() + "" + gracze.get(i).get(0).getKolor() + " ");
					System.out.println( gracze.get(i).get(1).getFigura() + "" + gracze.get(i).get(1).getKolor());
					System.out.println("Punkty: " + gracze.get(i).wyliczPunkty()); // wyswietl wynik
					if(!(gracze.get(i).czyBlackJack())) {         // czy gracz ma blackjacka ?
						wynikGraczy[i] = turaGracza(i);			 // jezeli nie to gra dalej
					}
					else {
						System.out.println("Gracz " + gracze.get(i).getNazwa() + " ma BlackJacka!");
						wynikGraczy[i]=21;
						pisz.get(i).println("punkty");						// gdy gracz ma blackjacka
						pisz.get(i).println("BLACKJACK!");					// informujemy go o tym
						pisz.get(i).println("blackjack");
					}
				} catch (Exception e){
					gracze.remove(i);
					polaczenie.remove(i);
					pisz.remove(i);
					czytaj.remove(i);
					iloscGraczy--;
				}
                for(int y=iloscGraczy-1; y>=0; y--){  //poinformuj że będą wysyłane wyniki innych graczy
                    try {
                        pisz.get(y).println("inni");
                        for(int x=0; x<gracze.get(i).getIloscKart(); x++){
                            pisz.get(y).print(gracze.get(i).get(x).getFigura()+ ""+ gracze.get(i).get(x).getKolor()+ " ");
                        }
                        pisz.get(y).println();
                    } catch (Exception e) {
                        gracze.remove(i);
                        polaczenie.remove(i);
                        pisz.remove(i);
                        czytaj.remove(i);
                        iloscGraczy--;
                    }
                }
			}
			// -------------------------------------------------------------------------
			
			// ------------------- tura krupiera ---------------------------------------
			if(iloscGraczy!=0){
				wynikKrupiera=turaKrupiera();
				System.out.println("Wynik krupiera to " + wynikKrupiera);
				System.out.println();
				for(int i=iloscGraczy-1; i>=0; i--){
					new WyslijGraczowi(i).start();
				}
				try {
					sleep(3000);			// odczekaj 3 sekundy zanim zostana ogloszone wyniki
				} catch (InterruptedException e) {e.printStackTrace();}
			}
			// -------------------------------------------------------------------------

			//-------------- sprawdzenie zwyciezcy --------------------------------
			for(int i=iloscGraczy-1; i>=0; i--){

				System.out.println("Wynik gracza " + gracze.get(i).getNazwa() + " to " + wynikGraczy[i]);
				if(wynikGraczy[i]>wynikKrupiera && wynikGraczy[i]>0){
                    System.out.println("Gracz wygral partie");
                    gracze.get(i).wygrana();
                }
                else if(wynikGraczy[i]==wynikKrupiera && wynikGraczy[i]>0){
                    System.out.println("Gracz zremisował");
                    gracze.get(i).remis();
                }
                else
                    System.out.println("Gracz przegrał");
				System.out.println();
			}
			try {
				sleep(2000);
			} catch (InterruptedException e) {	e.printStackTrace();}
			// -------------------------------------------------------------------------

			// -------------- rozdanie nagrod\aktualizacja stanu konta  ----------------
			for(int i=iloscGraczy-1; i>=0; i--){
				pisz.get(i).println("pieniadze");
				pisz.get(i).println(gracze.get(i).getPieniadze());
			}
			// ---------------------------------------------------------------------------
		}
		// ##############################################################################
	}   
	private static void ustawStrumienie(int i) throws IOException{
	    czytaj.add(new BufferedReader(new InputStreamReader(polaczenie.get(i).getInputStream())));   //do czytania
		pisz.add(new PrintWriter(polaczenie.get(i).getOutputStream(), true));             // do pisania
    }
}
