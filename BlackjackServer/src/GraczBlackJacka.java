/**
 * klasa wymaga używania kart, których figury zdefiniowane są jako A,2,3,4,5,6,7,8,9,10,J,Q,K 
 *
 */
public class GraczBlackJacka extends Gracz{
	private int	 	punkty;
	private int 	pieniadze;
	private int 	zaklad;
	public GraczBlackJacka() {
		super();
		this.punkty 	= 0;
		this.pieniadze 	= 1000;
		this.zaklad 	= 0;
	}
	
    /**
     * metoda zwraca wartosc true gdy pierwsze dwie karty to As i dowolna figura 
     * wynik ma znaczenie tylko przy pierwszym rozdaniu                          
     * wiec nie nalezy jej uzywac gdy gracz dobral juz kolejne karty           
     * @return true lub false w zależności od tego czy gracz ma BlackJacka
     */
	public boolean czyBlackJack() {
		try {
			if(get(0).getFigura()=='A')
				if(get(1).getFigura()=='J'
				|| get(1).getFigura()=='Q'
				|| get(1).getFigura()=='K')
					return true;
				
				else
					return false;
			else if (get(1).getFigura()=='A')
				if(get(0).getFigura()=='J'
				|| get(0).getFigura()=='Q'
				|| get(0).getFigura()=='K')
					return true;
				
				else
					return false;
			
			else  return false;
		} catch (Exception e) {		e.printStackTrace();	}
		return true;
	}
	/**
	 *  metoda oblicza i zwraca ilość punktów, które ma gracz, przyjmuję najlepszą możliwą sytuacje
	 *  dla gracza optymalnie licząć ilość punktów za posiadane asy, by o ile to możliwe osiągnąć jak najwięcej punktów
	 *  nie przekraczając jednocześnie 21
	 * @return ilość punktów gracza
	 */
	public int wyliczPunkty() {
		try {
			int iloscAsow=0;
			char wartoscKarty;   //wartosc pomocnicza
			punkty=0;
			for(int i=0; i<getIloscKart(); i++){
				wartoscKarty=get(i).getFigura();
				if(wartoscKarty=='A'){
					iloscAsow++;
					punkty+=11;
				}
				else if(wartoscKarty=='J'
					 || wartoscKarty=='Q'
					 || wartoscKarty=='K'
					 || wartoscKarty=='T')
					punkty+=10;
				else
					punkty+=Character.getNumericValue(wartoscKarty);
			}						
				while(iloscAsow>0 && punkty>21){      // jezeli byly asy w rece i liczba punktow
					iloscAsow--;                     // jest wieksza od 21, zmniejsza wartosc asow 
					punkty-=10;                     // z wartosci 11 na 1
				}
		} catch (Exception e) {	e.printStackTrace();	}
		return punkty;
	}
	
	/**
	 * 
	 * @return ilość pieniędzy, które posiada gracz
	 */
	public int getPieniadze() {
		return pieniadze;
	}
	/** Metoda zmienia stan konta gracza, jezeli gracz wygral w zwykly sposob otrzymuje dwa razy wiecej niz postawil
	 *  gdy gracz mial BlackJacka otrzymuje 2,5 raza wiecej niz postawil
	 *  metoda nie sprawdza kto wygral, nalezy jej uzywac tylko gdy gracz wygral partie
	 */
	public void wygrana() {
		if(czyBlackJack())
		this.pieniadze+=((this.zaklad*5)/2);
		else
			this.pieniadze+=this.zaklad*2;
	}
	/** Metoda zmienia stan konta gracza, jezeli gracz zremisował kwota jego zakładu wraca na konto
	 *  metoda nie sprawdza czy faktycznie wystapił remis, nalezy jej uzywac tylko gdy gracz zremisował partie
	 */
	public void remis() {
		this.pieniadze+=this.zaklad;
	}
	/**
	 * 
	 * @return zaklad - kwota postawionego zakładu
	 */
	public int getZaklad() {
		return zaklad;
	}
	/**
	 *  metoda nie sprawdza czy gracz ma wystarczającą ilość pieniedzy na postawienie zakładu, 
	 *  należy sprawdzić to samemu i używać tej metody tylko gdy gracz ma wystarczającą ilość pieniędzy
	 * @param zaklad - kwota zakladu, który gracz che postawić
	 */
	public void setZaklad(int zaklad) {
		this.pieniadze 	= this.pieniadze-zaklad;
		this.zaklad 	= zaklad;
	}
	/**
	 * metoda usuwa karty i punkty gracza
	 */
	public void noweRozdanie() {
		super.noweRozdanie();
		punkty=0;
	}

}
