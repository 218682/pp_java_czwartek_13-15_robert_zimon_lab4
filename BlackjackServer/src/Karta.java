/**
 * klasa definiuje pola i metody opisujące karte do gry
 * domyślnie przyjmowana jest wartość koloru i figury 0
 *
 */
public class Karta {
	private char Kolor;
	private char Figura;
	/**
	 * Konstruktor bezparametryczny 
	 * domyślnie przyjmowana jest wartość koloru i figury karty jako 0
	 */
	public Karta(){
		Kolor='0';
		Figura='0';
	}
	/**
	 * konstruktor przyjmujący dwa parametry
	 * @param Kolor - kolor tworzonej karty, zazwyczaj (C,S,D,H) ale można też 
	 * przyjąć inny schemat
	 * @param Figura - figura tworzonej karty, zazwyczaj A,2,3,4,5,6,7,8,9,10,J,Q,K 
	 * lecz w zależności od potrzeb można też zdefiniować inne figury
	 */
	public Karta(char Kolor, char Figura){
		this.Kolor=Kolor;
		this.Figura=Figura;
	}
	/**
	 * 
	 * @param K - kolor jaki ma mieć karta, zazwyczaj (C,S,D,H) ale można też 
	 * przyjąć inny schemat
	 */
	public void setKolor(char K){
		this.Kolor=K;
	}
	/**
	 * 
	 * @param F - figura karciana - zazwyczaj A,2,3,4,5,6,7,8,9,10,J,Q,K 
	 * lecz w zależności od potrzeb można też zdefiniować inne figury
	 */
	public void setFigura(char F) {
		this.Figura=F;
	}
	/**
	 * 
	 * @param K - kolor jaki ma mieć karta, zazwyczaj (C,S,D,H) ale można też 
	 * przyjąć inny schemat
	 * @param F - figura karciana - zazwyczaj A,2,3,4,5,6,7,8,9,10,J,Q,K 
	 * lecz w zależności od potrzeb można też zdefiniować inne figury
	 */
	public void set (char K, char F){
		
		this.Kolor=K;
		this.Figura=F;
	}
	/**
	 * 
	 * @param karta - karta, która ma zostać skopiowana
	 */
	public void set (Karta karta){
		
		this.Kolor=karta.getKolor();
		this.Figura=karta.getFigura();
	}
	/**
	 * 
	 * @return ustawiony kolor karty
	 */
	public char getKolor (){
		return Kolor;	
	}
	/**
	 * 
	 * @return ustawiona figura na karcie
	 */
	public char getFigura(){
		return Figura;
	}
}
