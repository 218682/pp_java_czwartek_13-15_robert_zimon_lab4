import java.util.Random;

/**
 *  klasa definiuje talie do gry,zawiera 52 karty, nie ma w niej Jokerów
 */
public class Talia {
	private Karta kartyWTalii[];
    
	// konstruktor tworzacy talie do gry
	public Talia() { 
		this.kartyWTalii = new Karta[52];
		for(int i=0; i<52; i++){
			this.kartyWTalii[i]= new Karta();
		}
		Karta karta = new Karta();
		char[] kolor={'C','S','D','H'}; //dostepne kolory dla kart Clubs, Spades, Diamonds, Hearts
		for(int i=1; i<14; i++){
			for(int j=0; j<4; j++){
				if(i==1){
					karta.set(kolor[j], 'A');
					this.kartyWTalii[(i-1)*4+j].set(karta);		  // przypadki gdy wartosc karty
							 									 // odpowiada ktorejs z figur
				}										        // lub nr 10 ktory jest dwoma	
				else if(i==10){							 	   // znakami
					karta.set(kolor[j], 'T');			
					this.kartyWTalii[(i-1)*4+j].set(karta);
				}
				else if(i==11){											
					karta.set(kolor[j], 'J');
					this.kartyWTalii[(i-1)*4+j].set(karta);
				}
				else if(i==12){
					karta.set(kolor[j], 'Q');
					this.kartyWTalii[(i-1)*4+j].set(karta);
				}
				else if(i==13){
					karta.set(kolor[j], 'K');
					this.kartyWTalii[(i-1)*4+j].set(karta);
				}
				else{
					karta.set(kolor[j], (char)(i+48));
					this.kartyWTalii[(i-1)*4+j].set(karta); // dla przypadkow zwyklych
				}
			}
		}
	}
	/**
	 * 
	 * @return metoda zwraca pełną talie do gry w karty
	 */
	public Karta[] getTalia() {
		return kartyWTalii;
	}
	/**
	 * 
	 * @return metoda zwraca losową karte z talii
	 */
	public Karta losujKarte() {
		Random k= new Random();
		int i;
		i=k.nextInt(52);
		return kartyWTalii[i];
	}

}
