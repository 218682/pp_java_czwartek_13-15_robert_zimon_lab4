import java.util.*;

public class Gracz {
	private LinkedList<Karta> listaKartWRece;
	private int 			  iloscKart;
	private String            nazwa;
	public Gracz() {
		this.listaKartWRece = new LinkedList<Karta>();
		iloscKart			= 0;
		nazwa               = new String();
		nazwa               = "Gracz1";
	}
	/**
	 * metoda usuwa karty gracza
	 */
	public void noweRozdanie() {
		while(iloscKart>0){
		this.listaKartWRece.remove(iloscKart-1);
		iloscKart--;
		}
	}
	/**
	 * 
	 * @param k - Karta do dodania do ręki gracza
	 */
	public void dodajKarte(Karta k) {
		this.listaKartWRece.add(k);
		iloscKart++;
	}
	/**
	 * 
	 * @param i - id karty z ręki gracza, numeracja od 0 
	 * @return karta - zwraca karte o podanym id
	 * @throws Exception - rzucany gdy nastąpi odwołanie do karty o nieprawidłowym id 
	 */
	public Karta get(int i)	throws Exception{
		if(i<iloscKart && i>=0)
		return listaKartWRece.get(i);
		else throw new Exception("Wystapil blad w odwolaniu do karty ktorej nie ma");
	}
	/**
	 * 
	 * @return ilosc kart posiadanych przez gracza
	 */
	public int getIloscKart()
	{
		return iloscKart;
	}
	/**
	 * ustawia nazwe gracza
	 * @param nazwa - nazwa gracza do ustawenia
	 */
	public void setNazwa(String nazwa)
	{
		this.nazwa=nazwa;
	}
	/**
	 * 
	 * @return zwraca nazwe gracza
	 */
	public String getNazwa()
	{
		return nazwa;
	}
}
